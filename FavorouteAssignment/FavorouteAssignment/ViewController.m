//
//  ViewController.m
//  FavorouteAssignment
//
//  Created by Said Rehouni on 25/09/14.
//  Copyright (c) 2014 Said Rehouni. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController () <UITextFieldDelegate, GMSMapViewDelegate>

@property (strong,nonatomic) GMSMapView *mapView;
@property (strong,nonatomic) UITextField *latitude;
@property (strong,nonatomic) UITextField *longitude;
@property (strong,nonatomic) UITextField *activeField;
@property (strong,nonatomic) UIButton *button;
@property float textfieldY;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUInteger widthView = self.view.frame.size.width;
    NSUInteger heightView = self.view.frame.size.height;
 
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:1.285
                                                            longitude:103.848
                                                                 zoom:12];
    
    CGRect latitudeRect = CGRectMake(20, heightView*0.85, widthView*0.6, 30);
    CGRect longitudeRect = CGRectMake(20, heightView*0.9, (widthView*0.6), 30);
    CGRect mapRect = CGRectMake(0,0, widthView, heightView*0.8);
    CGRect buttonRect = CGRectMake(widthView*0.7,heightView*0.872, widthView*0.25, 30);
    
    self.latitude = [[UITextField alloc] initWithFrame:latitudeRect];
    [self.latitude.layer setBorderWidth:1.0];
    
    self.longitude = [[UITextField alloc] initWithFrame:longitudeRect];
    [self.longitude.layer setBorderWidth:1.0];
    
    self.latitude.placeholder = @"latitude..";
    self.longitude.placeholder = @"longitude..";
    
    self.latitude.delegate = self;
    self.longitude.delegate = self;
    
    self.button = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.button setTitle:@"Button" forState:UIControlStateNormal];
    self.button.frame = buttonRect;
    [self.button addTarget:self action:@selector(pushAction:) forControlEvents:UIControlEventTouchUpInside];
    
    self.mapView = [[GMSMapView alloc] initWithFrame:mapRect];
    self.mapView.delegate = self;
    
    [self registerForKeyboardNotifications];
    
    [self.view addSubview:self.latitude];
    [self.view addSubview:self.button];
    [self.view addSubview:self.mapView];
    [self.view addSubview:self.longitude];

}

- (IBAction)pushAction:(UIButton *)sender
{
    float newLatitude = [self.latitude.text floatValue];
    float newLongitud = [self.longitude.text floatValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLatitude
                                                            longitude:newLongitud
                                                                 zoom:12];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLatitude,newLongitud);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    CGFloat hue = ( arc4random() % 256 / 256.0 );
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
    marker.icon = [GMSMarker markerImageWithColor:color];
    
    marker.map = self.mapView;
    [self.mapView setCamera:camera];
    
    [self textFieldInit];
}

- (void)textFieldInit
{
    self.latitude.text = @"";
    self.longitude.text = @"";
    self.latitude.placeholder = @"latitude..";
    self.longitude.placeholder = @"longitude..";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    marker.map = nil;
    
    return YES;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.textfieldY = textField.frame.origin.y + textField.frame.size.height;
    textField.placeholder = @"";
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:self.latitude] && [self.latitude.placeholder isEqual:@""] )
        self.latitude.placeholder = @"latitude..";
    
    else if ([textField isEqual:self.longitude] && [self.longitude.placeholder isEqual:@""] )
        self.longitude.placeholder = @"longitude..";
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - Keyboard events

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyBoardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if( self.textfieldY >= keyBoardSize.height ){
        CGRect viewWithKeyBoard = CGRectMake(0, keyBoardSize.height, self.view.bounds.size.width, self.view.bounds.size.height);
        [self.view setBounds:viewWithKeyBoard];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    CGRect viewWithKeyBoard = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
    [self.view setBounds:viewWithKeyBoard];
}

@end
